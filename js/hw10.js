// Завдання

let akali = document.querySelector('.tabs-title:nth-child(1)');
let notakali = Array.from(document.querySelectorAll('.tabs-title:not(.tabs-title:nth-child(1)'));
let notakaliinfo = Array.from(document.querySelectorAll('.person:not(.person:nth-child(1)'));
akali.addEventListener('click', function(){
    akali.classList.add('active');
    document.querySelectorAll('.tabs-content > li')[0].classList.add('shown');
    notakali.forEach(function(item){
        item.classList.remove('active');
    });
    notakaliinfo.forEach(function(item){
        item.classList.remove('shown');
    });
});

let anivia = document.querySelector('.tabs-title:nth-child(2)');
let notanivia = Array.from(document.querySelectorAll('.tabs-title:not(.tabs-title:nth-child(2)'));
let notaniviainfo = Array.from(document.querySelectorAll('.person:not(.person:nth-child(2)'));
anivia.addEventListener('click', function(){
    anivia.classList.add('active');
    document.querySelectorAll('.tabs-content > li')[1].classList.add('shown');
    notanivia.forEach(function(item){
        item.classList.remove('active');
    });
    notaniviainfo.forEach(function(item){
        item.classList.remove('shown');
    });
});

let draven = document.querySelector('.tabs-title:nth-child(3)');
let notdraven = Array.from(document.querySelectorAll('.tabs-title:not(.tabs-title:nth-child(3)'));
let notdraveninfo = Array.from(document.querySelectorAll('.person:not(.person:nth-child(3)'));
draven.addEventListener('click', function(){
    draven.classList.add('active');
    document.querySelectorAll('.tabs-content > li')[2].classList.add('shown');
    notdraven.forEach(function(item){
        item.classList.remove('active');
    });
    notdraveninfo.forEach(function(item){
        item.classList.remove('shown');
    });
});

let garen = document.querySelector('.tabs-title:nth-child(4)');
let notgaren = Array.from(document.querySelectorAll('.tabs-title:not(.tabs-title:nth-child(4)'));
let notgareninfo = Array.from(document.querySelectorAll('.person:not(.person:nth-child(4)'));
garen.addEventListener('click', function(){
    garen.classList.add('active');
    document.querySelectorAll('.tabs-content > li')[3].classList.add('shown');
    notgaren.forEach(function(item){
        item.classList.remove('active');
    });
    notgareninfo.forEach(function(item){
        item.classList.remove('shown');
    });
});

let kata = document.querySelector('.tabs-title:nth-child(5)');
let notkata = Array.from(document.querySelectorAll('.tabs-title:not(.tabs-title:nth-child(5)'));
let notkatainfo = Array.from(document.querySelectorAll('.person:not(.person:nth-child(5)'));
kata.addEventListener('click', function(){
    kata.classList.add('active');
    document.querySelectorAll('.tabs-content > li')[4].classList.add('shown');
    notkata.forEach(function(item){
        item.classList.remove('active');
    });
    notkatainfo.forEach(function(item){
        item.classList.remove('shown');
    });
});

